import uuid

import pytest

from src.tivus_data.aplicativo import Aplicativo
from src.tivus_data.conta import Conta
from src.tivus_data.engine import Engine

CONTA_ID = uuid.UUID('85BEF5BC-8203-46AE-99F1-357374E6C30C')


def test_cria_conta():
    db = Engine.mssql()
    conta = Conta(CONTA_ID, db)


def test_novo_aplicativo():
    engine = Engine.mssql()
    aplicativo = Aplicativo(uuid.uuid4(), engine)
    aplicativo.model.conta_id = CONTA_ID
    aplicativo.model.nome = "teste pycharm"
    aplicativo.model.comando = "comando"
    aplicativo.model.exit_code = '0'
    aplicativo.model.versao = '1.0'
    aplicativo.salvar()
    pytest.aplicativo_id = aplicativo.model.id


def test_editar_aplicativo():
    engine = Engine.mssql()
    aplicativo = Aplicativo(pytest.aplicativo_id, engine)
    aplicativo.model.arquitetura = 32
    aplicativo.model.Usuario = True
    aplicativo.model.Habilitado = False
    aplicativo.salvar()


def test_excluir_aplicativo():
    engine = Engine.mssql()
    aplicativo = Aplicativo(pytest.aplicativo_id, engine)
    aplicativo.excluir()



