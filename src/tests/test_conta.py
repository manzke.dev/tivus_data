import uuid
import pytest
from src.tivus_data.conta import Conta
from src.tivus_data.engine import Engine


def test_conta_insert():
    engine = Engine.mssql()
    conta = Conta(uuid.uuid4(), engine)
    conta.salvar()
    pytest.conta_id = conta.model.id


def test_conta_delete():
    engine = Engine.mssql()
    conta = Conta(pytest.conta_id, engine)
    conta.excluir()
