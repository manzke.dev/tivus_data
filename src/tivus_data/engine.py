import os
from sqlalchemy import create_engine


class Engine:

    TIVUS_DATABASE_HOST = os.environ.get('TIVUS_DATABASE_HOST')
    TIVUS_DATABASE_NAME = os.environ.get('TIVUS_DATABASE_NAME')
    TIVUS_DATABASE_USER = os.environ.get('TIVUS_DATABASE_USER')
    TIVUS_DATABASE_PASSWORD = os.environ.get('TIVUS_DATABASE_PASSWORD')

    @staticmethod
    def mssql():
        return create_engine(
            f'mssql+pyodbc://{Engine.TIVUS_DATABASE_USER}:{Engine.TIVUS_DATABASE_PASSWORD}@{Engine.TIVUS_DATABASE_HOST}/{Engine.TIVUS_DATABASE_NAME}?driver=ODBC Driver 13 for SQL Server')



