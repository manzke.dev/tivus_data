import sqlalchemy
from sqlalchemy.orm import Session


class Base:

    def __init__(self, model, model_id, engine: sqlalchemy.engine.Engine):
        self._engine = engine
        self._session = Session(engine, future=True)
        self.model = self._session.query(model).filter_by(id=model_id).first()
        if self.model is None:
            self.model = model()
        self._session.add(self.model)

    def salvar(self):
        try:
            self._session.commit()
            return True
        except Exception:
            return False

    def excluir(self):
        self._session.delete(self.model)
        self._session.commit()
