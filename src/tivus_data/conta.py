import sqlalchemy
import tivus_models.models
from sqlalchemy.orm import Session
from tivus_models.models import Conta as Model

from src.tivus_data.base import Base


class Conta(Base):

    def __init__(self,  conta_id, engine: sqlalchemy.engine.Engine):
        super().__init__(Model, conta_id, engine)
