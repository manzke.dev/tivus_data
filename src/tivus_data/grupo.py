import sqlalchemy.engine
from tivus_models.models import Grupo as Model
from src.tivus_data.base import Base


class Grupo(Base):

    def __init__(self, grupo_id, engine: sqlalchemy.engine.Engine):
        super().__init__(grupo_id, engine)
        self.model = self._session.query(Model).filter_by(id=grupo_id).first()
        if self.model is None:
            self.model = Model()
        self._session.add(self.model)

